#!/bin/bash
#
# By JMarceloFN - https://gitlab.com/JMarceloFN/
# First part of the process of installing the wi-fi card for the laptop
# Dell XPS 1330. You have some experience, you can skip this part and
# use helpers like yay or paru or do manually. It's up to you.
# At the end of this script, there's no need to reboot, go to script
# number two.
# Be sure to have "git" package installed first.
# Always check the code, don't trust anyone.

mkdir ~/dell-xps-1330-wi-fi-arch-linux
cd ~/dell-xps-1330-wi-fi-arch-linux
git clone https://aur.archlinux.org/b43-firmware-classic.git
cd b43-firmware-classic 
makepkg -si
cd ..
rm -rf ~/dell-xps-1330-wi-fi-arch-linux
echo "#*#*#*#*#*#* Go to part 2 of the install process *#*#*#*#*#*#"
