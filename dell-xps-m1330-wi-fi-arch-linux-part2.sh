#!/bin/bash
#
# By JMarceloFN - https://gitlab.com/JMarceloFN/
# Second part of the process of installing the wi-fi card for the laptop
# Dell XPS 1330. For this part you need to be used as sudo.
# Always check the code, don't trust anyone.


WL=/usr/lib/modprobe.d/broadcom-wl.conf
DKMS=/usr/lib/modprobe.d/broadcom-wl-dkms.conf

touch /etc/modules-load.d/b43.conf
echo b43 >> /etc/modules-load.d/b43.conf

    if
        [[ -f "$WL" ]];
        then
            sed -e '/\bb43\b/ s/^#*/#/' -i $WL 
        else
            echo "Broadcom-wl isn't blacklisted"
    fi

    if
        [[ -f "$DKMS" ]];
        then
            sed -e '/\bblacklist b43\b/ s/^#*/#/' -i $DKMS
         else
            echo "Broadcom-wl-dkms isn't blacklisted"
    fi        

echo "Reboot the laptop and make sure that the physical wi-fi button \
on the right side of the case is turned on or this button is configured \
at bios to not be used."
