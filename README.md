**Wi-Fi drivers for Broadcom BCM4311 - Dell XPS M1330** \
\
There are two scripts, the first one is for installing \
the drivers for Broadcom BCM4311, no need to execute as \
sudo. \
The second script is to be sure to autoload the module and \
see if isn't blacklisted. This one needs to be executes as \
sudo. \
After, reboot and the Wi-Fi card should be working. \
Be sure that the physical wi-fi button on the right side of \
the case is turned on or this button is configured at bios to 
not be used.
